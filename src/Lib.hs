module Lib
  ( mainFunc
  ) where

import qualified Data.Attoparsec.Lazy  as AP
import qualified Data.ByteString       as BL
import qualified Data.ByteString.Char8 as BC
import           Data.MarkovChain
import qualified Data.Text.Encoding    as T
import           System.Environment
import           System.Exit
import           System.Random
import           Web.Twitter.Conduit   hiding (map)
import           Web.Twitter.Types     (statusText)

import           Lib.Prelude
import           Lib.Tw
import           Lib.Wasap

mainFunc :: IO ()
mainFunc = do
  randomSeed <- getStdGen
  args <- getArgs
  twInfo <- getTwInfoFromProxy
  mgr <- newManager tlsManagerSettings
  case args of
    file:namen -> do
      fileContent <- parseFile file
      let chats =
            (map content) .
            (filter (\bericht -> sender bericht `elem` map BC.pack namen)) $
            fileContent
      res <-
        call twInfo mgr $
        update $ T.decodeUtf8 $ generateBullshit chats randomSeed
      putStrLn $ statusText res
    _ -> do
      putStrLn ("<this program> <file archive> <usernames>" :: Text)
      exitFailure

parseFile :: FilePath -> IO ChatLog
parseFile filename = do
  fileContent <- BL.readFile filename
  case AP.parseOnly parserChatLog fileContent of
    Right chats -> return chats
    Left _      -> pure []

generateBullshit :: [[ByteString]] -> StdGen -> ByteString
generateBullshit fileContent randomSeed =
  BL.intercalate " " $ take 20 $ concat $ runMulti 1 fileContent 0 randomSeed
