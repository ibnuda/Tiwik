module Lib.Tw where

import           Protolude             hiding (Proxy)

import           Control.Lens
import qualified Data.ByteString.Char8 as S8
import qualified Data.CaseInsensitive  as CI
import qualified Data.Map              as M
import           Network.HTTP.Conduit
import qualified Network.URI           as URI
import           System.Environment
import           Text.Read
import           Web.Twitter.Conduit

getOAuthToken :: IO (OAuth, Credential)
getOAuthToken = do
  consumerKey <- getEnv' "OAUTH_CONSUMER_KEY"
  consumerSecret <- getEnv' "OAUTH_CONSUMER_SECRET"
  accessToken <- getEnv' "OAUTH_ACCESS_TOKEN"
  accessSecret <- getEnv' "OAUTH_ACCESS_SECRET"
  let oauth =
        twitterOAuth
        { oauthConsumerKey = consumerKey
        , oauthConsumerSecret = consumerSecret
        }
      cred =
        Credential [ ("oauth_token", accessToken)
                   , ("oauth_token_secret", accessSecret)
                   ]
  return (oauth, cred)
  where
    getEnv' = (S8.pack <$>) . getEnv

getProxyEnv :: IO (Maybe Proxy)
getProxyEnv = do
  env <- M.fromList . over (mapped . _1) CI.mk <$> getEnvironment
  let u =
        M.lookup "https_proxy" env <|> M.lookup "http_proxy" env <|> M.lookup "proxy" env >>=
        URI.parseURI >>=
        URI.uriAuthority
  return $ Proxy <$> (S8.pack . URI.uriRegName <$> u) <*> (parsePort . URI.uriPort <$> u)
  where
    parsePort :: [Char] -> Int
    parsePort []       = 8080
    parsePort (':':xs) = read xs
    parsePort _        = undefined

getTwInfoFromProxy :: IO TWInfo
getTwInfoFromProxy = do
  prox <- getProxyEnv
  (oa, cred) <- getOAuthToken
  return $ (setCredential oa cred def) {twProxy = prox}
