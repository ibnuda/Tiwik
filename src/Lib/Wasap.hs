module Lib.Wasap
  ( Bericht (..)
  , ChatLog
  , parserChatLog
  ) where

import           Control.Applicative
import qualified Data.Attoparsec.ByteString.Char8 as BC
import           Data.Attoparsec.Combinator       (lookAhead)
import           Data.Attoparsec.Lazy             as AL
import           Data.ByteString.Char8            as BS hiding (concat, count,
                                                         map, take)
import           Data.Time
import           Protolude                        hiding (take)
import           Text.Read

data Bericht = Bericht
  { datum   :: UTCTime
  , sender  :: ByteString
  , content :: [ByteString]
  } deriving (Show, Eq)

type ChatLog = [Bericht]

parserDatum :: Parser UTCTime
parserDatum = do
  dd <- count 2 BC.digit
  _ <- BC.char '/'
  mm <- count 2 BC.digit
  _ <- BC.char '/'
  yyyy <- count 4 BC.digit
  _ <- string ", "
  hh <- count 2 BC.digit
  _ <- BC.char ':'
  m <- count 2 BC.digit
  _ <- string " - "
  pure $
    UTCTime
    { utctDay = fromGregorian (read yyyy) (read mm) (read dd)
    , utctDayTime = secondsToDiffTime $ (read hh) * 3600 + (read m) * 60
    }

isDatumAhead :: Parser ()
isDatumAhead = lookAhead parserDatum *> pure ()

parserVerzender :: Parser ByteString
parserVerzender = do
  BC.takeTill (== ':')

parserPraat :: Parser ByteString
parserPraat = do
  rest <- BC.takeTill (== '\n')
  end <- atEnd
  if end
    then pure rest
    else (BC.char '\n') >> pure rest

splitAtSpace :: ByteString -> [ByteString]
splitAtSpace = BS.split (' ')

parserBericht :: Parser Bericht
parserBericht = do
  date <- parserDatum
  crimineel <- parserVerzender
  _ <- take 2
  a <- parserPraat
  b <- manyTill parserPraat $ endOfInput <|> isDatumAhead
  pure $ Bericht date crimineel $ concat $ splitAtSpace a : map splitAtSpace b

parserChatLog :: Parser ChatLog
parserChatLog = many parserBericht
